class BorrowsController < ApplicationController
  # TODO before_actions for certain actions? requires implementation of authentication
  
  def index
    @user_options = User.all.map { |a| [ a.name, a.id ] }

    # filter by borrower or borrowing, if given
    scope = Borrow
    if (params[:borrower].present?)
      scope = scope.where("borrower_id = ?", params[:borrower])
    end
    if (params[:borrowing].present?)
      scope = scope.where("borrowing_id = ?", params[:borrowing])
    end
    if (!params[:repaid].present?)
      scope = scope.where.not(repaid: true)
    end
    @borrows = scope.all.paginate(page: params[:page])
  end

  def show
    @borrow = Borrow.find_by(id: params[:id])
  end

  def new
    @borrow = Borrow.new
    @borrow.date = Time.zone.now
  end

  def create
    @borrower = User.find_by(id: params[:borrow][:borrower_id])
    @borrow = @borrower.active_borrows.build(safe_params)
    @borrow.repaid = false
    if @borrow.save
      flash[:success] = "Borrow created!"
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @borrow = Borrow.find_by(id: params[:id])
  end

  def update
    @borrow = Borrow.find_by(id: params[:id])
    if @borrow.update_attributes(safe_params)
      flash[:success] = "Borrow updated!"
      redirect_to root_url
    else
      render 'edit'
    end
  end

  def destroy
    Borrow.find_by(id: params[:id]).destroy
    flash[:success] = "Borrow destroyed!"
    redirect_to borrows_url
  end
  
  private
  
    def safe_params
      params.require(:borrow).permit(:borrower_id, :borrowing_id, :amount, :date, :comment, :repaid)
    end
end
