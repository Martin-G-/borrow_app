class UsersController < ApplicationController
  # TODO before_actions for certain actions? requires implementation of authentication

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find_by(id: params[:id])
    @sums_to = @user.borrows_to_sums
    @sums_from = @user.borrowing_from_sums
    @totals = @user.totals
  end

  def new
    @user = User.new
  end

  def create
    @user = User.create(safe_params)
    if @user.save
      flash[:success] = "User created!"
      redirect_to users_url
    else
      render 'new'
    end
  end

  def edit
    @user = User.find_by(id: params[:id])
  end
  
  def update
    @user = User.find_by(id: params[:id])
    if @user.update_attributes(safe_params)
      flash[:success] = "User updated!"
      redirect_to users_url
    else
      render 'edit'
    end
  end
  
  def destroy
    User.find_by(id: params[:id]).destroy
    flash[:success] = "User destroyed!"
    redirect_to users_url
  end
  
  # TODO warum kein eigener controller?
  def totals
    @totals = Hash.new(0)
    # TODO repaid rein
    Borrow.all.each do |b|
      @totals[b.borrower] -= b.amount
      @totals[b.borrowing] += b.amount
    end
    @totals = @totals.reject { |user, amount| amount == 0 }
    # solve
    @solve = []
    t = @totals.clone
    while t.count > 0
      max_t = t.max_by { |user, amount| amount }
      min_t = t.min_by { |user, amount| amount }
      if max_t[1] + min_t[1] > 0
        @solve << { user_from: max_t[0], user_to: min_t[0], amount: -min_t[1] }
        t[max_t[0]] += min_t[1]
        t[min_t[0]] = 0
      else
        @solve << { user_from: max_t[0], user_to: min_t[0], amount: max_t[1] }
        t[min_t[0]] += max_t[1]
        t[max_t[0]] = 0
      end
      t = t.reject { |user, amount| amount == 0 }
    end
  end
  
  private
  
    def safe_params
      params.require(:user).permit(:name)
    end
end
