class User < ActiveRecord::Base
  validates :name, presence: true, allow_nil: false, uniqueness: true, length: { minimum: 3, maximum: 100 }
  
  # TODO wirklich destroy - sollen Borrows gelöscht werden, wenn ein User gelöscht wird?
  has_many :active_borrows, class_name: "Borrow", foreign_key: "borrower_id", dependent: :destroy
  has_many :passive_borrows, class_name: "Borrow", foreign_key: "borrowing_id", dependent: :destroy
  
  def borrow(other_user, amount, comment, date)
    active_borrows.create(borrowing_id: other_user.id, amount: amount, comment: comment, date: date, repaid: false)
  end
  
  def unborrow(id)
    active_borrows.find_by(id: id).destroy
  end

  def borrows_to(other_user)
    active_borrows.where(repaid: false).where(borrowing: other_user)
  end

  def borrowing_from(other_user)
    passive_borrows.where(repaid: false).where(borrower: other_user)
  end

  def borrows_to_sums
    active_borrows.where(repaid:false).group('borrowing').sum(:amount)
  end

  def borrowing_from_sums
    passive_borrows.where(repaid: false).group('borrower').sum(:amount)
  end

  def totals
    t = Hash.new(0)
    # negative values imply that the other user borrowes more
    # to this user than this user borrowed from the other user
    borrows_to_sums.each do |user, sum|
      t[user] += sum
    end
    borrowing_from_sums.each do |user, sum|
      t[user] -= sum
    end
    # if amount is zero, remove this entry
    t.reject { |user, sum| sum == 0 }
  end
end
