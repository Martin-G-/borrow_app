class Borrow < ActiveRecord::Base
  validates :borrower_id, presence: true, allow_nil: false
  validates :borrowing_id, presence: true, allow_nil: false
  validate { errors.add(:base, 'Borrower and Borrowing must be different!') if borrower_id == borrowing_id }
  validates :amount, presence: true, allow_nil: false, numericality: { greater_than: 0 }
  # validates :comment
  validates :date, presence: true, allow_nil: false
  # validates :repaid

  belongs_to :borrowing, class_name: "User"
  belongs_to :borrower, class_name: "User"
end
