require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @one = users(:one)
    @two = users(:two)
  end
  
  test "should be valid" do
    assert @one.valid?
  end
  
  test "should not allow empty" do
    @one.name = nil
    assert_not @one.valid?
    @one.name = "   "
    assert_not @one.valid?
  end
  
  test "should be unique" do
    @one.save
    @two.name = @one.name
    assert_not @two.valid?
  end
  
  test "should check name length" do
    @one.name = "x"
    assert_not @one.valid?
    @one.name = "x" * 101
    assert_not @one.valid?
  end
  
  test "should borrow and unborrow" do
    assert_empty @one.borrows_to(@two)
    assert_empty @two.borrowing_from(@one)
    borrow = @one.borrow(@two, 10.00, "Test borrow", Time.zone.now)
    assert @one.borrows_to(@two).include?(borrow)
    assert @two.borrowing_from(@one).include?(borrow)
    @one.unborrow(borrow)
    assert_empty @one.borrows_to(@two)
    assert_empty @two.borrowing_from(@one)
  end

  test "should not list repaid borrows" do
    borrow = @one.borrow(@two, 10.00, "Test borrow", Time.zone.now)
    assert @one.borrows_to(@two).include?(borrow)
    assert @two.borrowing_from(@one).include?(borrow)
    assert_equal 10.00, @one.totals[@two]
    borrow.repaid = true
    borrow.save
    assert_empty @one.borrows_to(@two)
    assert_empty @two.borrowing_from(@one)
    assert_nil @one.totals[@two]
  end

  test "associated borrows should be destroyed when user is destroyed" do
    @one.save
    @two.save
    @one.borrow(@two, 10.0, "Test borrow", Time.zone.now)
    assert_difference 'Borrow.count', -1 do
      @one.destroy
    end
  end
end
