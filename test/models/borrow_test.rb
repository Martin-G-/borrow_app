require 'test_helper'

class BorrowTest < ActiveSupport::TestCase
  def setup
    @one = users(:one)
    @two = users(:two)
  end
  
  test "should be valid" do
    @borrow = @one.borrow(@two, 10.5, "Test Borrow", Time.zone.now)
    assert @borrow.valid?
  end
  
  test "should not allow empty" do
    @borrow = @one.borrow(@two, 10.5, "Test Borrow", Time.zone.now)
    @borrow.borrower_id = nil
    assert_not @borrow.valid?
    @borrow.borrower_id = @one.id

    @borrow.borrowing_id = nil
    assert_not @borrow.valid?
    @borrow.borrowing_id = @two.id

    @borrow.amount = nil
    assert_not @borrow.valid?
    @borrow.amount = 10.5

    @borrow.date = nil
    assert_not @borrow.valid?
    @borrow.date = Time.zone.now
    assert @borrow.valid?
  end
  
  test "amount should be greater than 0" do
    @borrow = @one.borrow(@two, 10.5, "Test Borrow", Time.zone.now)
    @borrow.amount = -5.42
    assert_not @borrow.valid?
  end
  
  test "borrower should not be borrowing" do
    @borrow = @one.borrow(@two, 10.5, "Test Borrow", Time.zone.now)
    @borrow.borrowing_id = @borrow.borrower_id
    assert_not @borrow.valid?
  end
end
