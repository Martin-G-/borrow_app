require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  def setup
    @one = users(:one)
  end
  
  test "should get index" do
    get :index
    assert_response :success
    assert_match @one.name, response.body
  end

  test "should get show" do
    get :show, id: @one.id
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get create" do
    post :create, user: { name: "Test User" }
    assert_redirected_to users_url
  end
  
  test "should get edit" do
    get :edit, id: @one.id
    assert_response :success
  end

  test "should get update" do
    put :update, id: @one.id, user: { name: "Test User" }
    assert_redirected_to users_url
  end
  
  test "should get destroy" do
    delete :destroy, id: @one.id
    assert_redirected_to users_url
  end
end
