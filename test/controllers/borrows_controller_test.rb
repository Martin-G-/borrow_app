require 'test_helper'

class BorrowsControllerTest < ActionController::TestCase
  def setup
    @one = users(:one)
    @two = users(:two)
    @borrow = @one.borrow(@two, 10.01, "Test borrow", Time.zone.now)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_match @one.name, response.body
  end

  test "should get show" do
    get :show, id: @borrow.id
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get create" do
    post :create, borrow: { borrower_id: @one.id, borrowing_id: @two.id,
      amount: 43.21, comment: "Test borrow", date: 5.minutes.ago}
    assert_redirected_to root_url
  end

  test "should get edit" do
    get :edit, id: @borrow.id
    assert_response :success
  end

  test "should get update" do
    put :update, id: @borrow.id, borrow: { borrower_id: @one.id, borrowing_id: @two.id,
      amount: 43.21, comment: "Test borrow", date: 5.minutes.ago}
    assert_redirected_to root_url
  end

  test "should get destroy" do
    delete :destroy, id: @borrow.id
    assert_redirected_to borrows_url
  end
end
