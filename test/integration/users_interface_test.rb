require 'test_helper'

class UsersInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:one)
  end
  
  test "user interface" do
    # new
    get new_user_path
    assert_template 'users/new'
    # create - invalid
    assert_no_difference 'User.count' do
      post users_path, user: { name: "x" }
    end
    assert_select 'div#error_explanation'
    # create - valid
    assert_difference 'User.count', 1 do
      post users_path, user: { name: "Test User" }
    end
    assert_redirected_to users_url
    follow_redirect!
    # index
    assert_template 'users/index'
    assert_match @user.name, response.body
    # show
    @user = User.first
    get user_path(@user)
    assert_template 'users/show'
    assert_match @user.name, response.body
    # edit
    get edit_user_path(@user)
    assert_template 'users/edit'
    # update - invalid
    put user_path(@user), id: @user.id, user: { name: "x" }
    assert_select 'div#error_explanation'
    # update - valid
    put user_path(@user), id: @user.id, user: { name: "Test User new" }
    assert_redirected_to users_url
    # delete
    assert_difference 'User.count', -1 do
      delete user_path(@user)
    end
  end
end
