require 'test_helper'

class BorrowsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @one = users(:one)
    @two = users(:two)
  end
  
  test "borrow interface" do
    # new
    get new_borrow_path
    assert_template 'borrows/new'
    assert_match @one.name, response.body
    # create - invalid
    assert_no_difference 'Borrow.count' do
      post borrows_path, borrow: { borrower_id: @one.id, borrowing_id: @two.id,
      amount: -1.00, comment: "Invalid borrow", date: 5.days.ago }
    end
    assert_select 'div#error_explanation'
    # create - valid
    assert_difference 'Borrow.count', 1 do
      post borrows_path, borrow: { borrower_id: @one.id, borrowing_id: @two.id,
      amount: 1.00, comment: "Valid borrow", date: 5.seconds.ago }
    end
    assert_redirected_to root_url
    follow_redirect!
    # index
    assert_template 'borrows/index'
    assert_match @one.name, response.body
    # show
    @borrow = Borrow.first
    get borrow_path(@borrow)
    assert_template 'borrows/show'
    assert_match @borrow.borrower.name, response.body
    assert_match @borrow.borrowing.name, response.body
    assert_match @borrow.amount.to_s, response.body
    assert_match @borrow.date.to_formatted_s(:daymonthyear), response.body
    assert_match @borrow.comment, response.body
    # edit
    get edit_borrow_path(@borrow)
    assert_template 'borrows/edit'
    # update - invalid
    put borrow_path(@borrow), id: @borrow.id, borrow: { borrower_id: @two.id,
      borrowing_id: @two.id, amount: 0.0, comment: "Invalid borrow",
      date: 5.years.ago }
    assert_select 'div#error_explanation'
    # update - valid
    put borrow_path(@borrow), id: @borrow.id, borrow: { borrower_id: @two.id,
      borrowing_id: @one.id, amount: 5.0, comment: "Valid borrow",
      date: 5.seconds.ago }
    assert_redirected_to root_url
    # delete
    assert_difference 'Borrow.count', -1 do
      delete borrow_path(@borrow)
    end
  end

  test "borrow on users#show" do
    get user_path(@one)
    assert_no_match @two.name, response.body
    assert_difference 'Borrow.count', 1 do
      post borrows_path, borrow: { borrower_id: @one.id, borrowing_id: @two.id,
      amount: 1.09, comment: "My borrow", date: 5.seconds.ago }
    end
    get user_path(@one)
    assert_match @two.name, response.body
    assert_match "1.09", response.body
  end

  test "borrow interface repaid" do
    assert_difference 'Borrow.count', 1 do
      post borrows_path, borrow: { borrower_id: @one.id, borrowing_id: @two.id,
      amount: 1.00, comment: "My not repaid borrow", date: 5.seconds.ago }
    end
    @borrow = Borrow.first
    assert_equal false, @borrow.repaid
    get borrows_path
    assert_match "My not repaid borrow", response.body
    get user_path(@one)
    assert_match @two.name, response.body
    put borrow_path(@borrow), id: @borrow.id, borrow: { 
      comment: "My repaid borrow", repaid: true }
    @borrow = Borrow.first
    assert_equal true, @borrow.repaid
    get borrows_path
    assert_no_match "My repaid borrow", response.body
    get user_path(@one)
    assert_no_match @two.name, response.body
    get borrows_path, repaid: 1
    assert_match "My repaid borrow", response.body
  end
end
