date_formats = {
  daymonthyear: '%d.%m.%Y' # 20.01.2015
}

Time::DATE_FORMATS.merge! date_formats
Date::DATE_FORMATS.merge! date_formats
