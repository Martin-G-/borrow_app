class CreateBorrows < ActiveRecord::Migration
  def change
    create_table :borrows do |t|
      t.integer :borrower_id
      t.integer :borrowing_id
      t.decimal :amount
      t.text :comment
      t.datetime :date

      t.timestamps null: false
    end
    add_index :borrows, :borrower_id
    add_index :borrows, :borrowing_id
  end
end
