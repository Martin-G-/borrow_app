class AddRepaidToBorrow < ActiveRecord::Migration
  def change
    add_column :borrows, :repaid, :boolean
  end
end
