# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

one = User.create(name: "Peter Parker")
two = User.create(name: "Ute Umläüt")
three = User.create(name: "Joachim Joghurt")

Borrow.create(borrower_id: one.id, borrowing_id: two.id, amount: 12.34, 
  comment: "Brötchen", date: Time.zone.now)
Borrow.create(borrower_id: two.id, borrowing_id: three.id, amount: 51.78, 
  comment: "10 Pfund Kaffee", date: 3.days.ago)
Borrow.create(borrower_id: three.id, borrowing_id: one.id, amount: 1.50, 
  date: 3.days.ago)
